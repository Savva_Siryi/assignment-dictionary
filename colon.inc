%macro colon 2
    %ifid %2
        %ifstr %1
            %2:
            %ifdef next_elem
                .next: dq next_elem
            %else
                .next: dq 0
            %endif
            %define next_elem %2
            .key: db %1, 0
            .value:
        %else
            %fatal "Not str"
        %endif
    %else
        %fatal "Not id"
    %endif
%endmacro