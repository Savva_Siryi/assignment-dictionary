global find_word
%include "lib.inc"

section .text

find_word:
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .success
    .fail:
        cmp [rsi], word 0
        jne .continue
        xor rax, rax
        ret
    .continue:
        mov rsi, [rsi]
        jmp find_word
    .success:
        call string_length
        add rsi, 8
        add rsi, rax
        inc rsi
        mov rax, rsi
        ret
