global _start

%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%include "define.inc"

%define BUFFER_SIZE 256

section .data
input_buffer: times BUFFER_SIZE db 0x00

section .rodata
input_greet: db 'please enter key: ', 0
input_fail_message: db 'key is not valid (empty or too long)', new_line, 0
find_fail_message: db "no such key in map", new_line, 0
new_line_mes: db new_line, 0

section .text
_start:
    mov rdi, input_greet
    call print_string_err
    mov rsi, BUFFER_SIZE
    mov rdi, input_buffer
    call read_word
    test rax, rax 
    jz .input_fail
    mov rdi, input_buffer
    mov rsi, next_elem
    call find_word
    test rax, rax
    jz .find_fail
    mov rdi, rax
    call print_string
    mov rdi, new_line_mes,
    call print_string
    xor rdi, rdi
    call exit

.find_fail:
    mov rdi, find_fail_message
    call print_string_err
    mov rdi, 3
    call exit

.input_fail:
    mov rdi, input_fail_message
    call print_string_err
    mov rdi, 2
    call exit